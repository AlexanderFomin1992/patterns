#include <vector>
#include <stdio.h>

class Unit {
public:
	virtual int getPayload() = 0;
	Unit * getComposite() {
		return 0;
	}
};

class Composite: public Unit {
public:
	int getPayload() {
		 // Проход по вектору с выводом значений
		int fullPayload = 0;
	    for ( Unit * unit : units ) {
	    	fullPayload += unit->getPayload();
	    }
	    return fullPayload;
	}
	Unit * getComposite() {
		return this;
	}
	void add(Unit * unit) {
		units.push_back(unit);
	}

private:
	std::vector <Unit *> units = {};
};

class Truck: public Unit {
public:
	int getPayload() {
		return 5000;
	}
};

class PassengerCar: public Unit {
public:
	int getPayload() {
		return 400;
	}
};

int main() {

	Composite * garageRussia = new Composite();
	Composite * garageMoscow = new Composite();
	Composite * garageSaintPetesburg = new Composite();
	garageRussia->add(garageMoscow);
	garageRussia->add(garageSaintPetesburg);

	Truck * truck_1 = new Truck();
	Truck * truck_2 = new Truck();
	Truck * truck_3 = new Truck();
	PassengerCar * passengerCar_1 = new PassengerCar();
	PassengerCar * passengerCar_2 = new PassengerCar();
	PassengerCar * passengerCar_3 = new PassengerCar();

	garageMoscow->add(truck_1);
	garageMoscow->add(truck_2);
	garageMoscow->add(passengerCar_1);

	garageSaintPetesburg->add(truck_3);
	garageSaintPetesburg->add(passengerCar_2);
	garageSaintPetesburg->add(passengerCar_3);

	fprintf(stderr, "Russia garage payload: %d\r\n", garageRussia->getPayload());
	fprintf(stderr, "Moscow garage payload: %d\r\n", garageMoscow->getPayload());
	fprintf(stderr, "Saint Petersburg garage payload: %d\r\n", garageSaintPetesburg->getPayload());

	return 0;
}