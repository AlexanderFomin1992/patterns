#include <stdio.h>


class Archer {
public:
	virtual void info() = 0;
};

class Swordman {
public:
	virtual void info() = 0;
};

class Horseman {
public:
	virtual void info() = 0;
};


class RomanArcher: public Archer {
public:
	void info() {
		fprintf(stderr, "I'am a roman Archer!\r\n");
	}
};


class RomanSwordman: public Swordman {
public:
	void info() {
		fprintf(stderr, "I'am a roman Swordman!\r\n");
	}
};

class RomanHorseman: public Horseman {
public:
	void info() {
		fprintf(stderr, "I'am a roman Horseman!\r\n");
	}
};

class CartageArcher: public Archer {
public:
	void info() {
		fprintf(stderr, "I'am a cartage Archer!\r\n");
	}
};


class CartageSwordman: public Swordman {
public:
	void info() {
		fprintf(stderr, "I'am a cartage Swordman!\r\n");
	}
};

class CartageHorseman: public Horseman {
public:
	void info() {
		fprintf(stderr, "I'am a cartage Horseman!\r\n");
	}
};

class Barracks {
public:
	virtual Archer * createArcher() = 0;
	virtual Swordman * createSwordman() = 0;
	virtual Horseman * createHorseman() = 0;
};

class RomanBarracks: public Barracks {
	Archer * createArcher() { return new RomanArcher(); };
	Swordman * createSwordman() { return new RomanSwordman(); };
	Horseman * createHorseman() { return new RomanHorseman(); };
};

class CartageBarracks: public Barracks {
	Archer * createArcher() { return new CartageArcher(); };
	Swordman * createSwordman() { return new CartageSwordman(); };
	Horseman * createHorseman() { return new CartageHorseman(); };
};

int main() {
	Barracks * romanBarracks = new RomanBarracks();
	Archer * romanArcher = romanBarracks->createArcher();
	Swordman * romanSwordman = romanBarracks->createSwordman();
	Horseman * romanHorseman = romanBarracks->createHorseman();

	Barracks * cartageBarracks = new CartageBarracks();
	Archer * cartageArcher = cartageBarracks->createArcher();
	Swordman * cartageSwordman = cartageBarracks->createSwordman();
	Horseman * cartageHorseman = cartageBarracks->createHorseman(); 

	romanArcher->info();
	romanSwordman->info();
	romanHorseman->info();

	cartageArcher->info();
	cartageSwordman->info();
	cartageHorseman->info();
}