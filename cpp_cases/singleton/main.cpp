#include "Singleton.h"
#include <stdio.h>

int main(int argc, char const *argv[])
{
	Singleton * my_instance_1 = Singleton::getInstance();
	Singleton * my_instance_2 = Singleton::getInstance();
	Singleton * my_instance_3 = Singleton::getInstance();

	/* code */
	return 0;
}