#include <stdio.h>

class Singleton {
private:
	Singleton() {}
	static Singleton * p_instance;
public:
	static Singleton * getInstance() {
		if (!p_instance) {
			fprintf(stderr, "%s\n", "Create new instance");
			p_instance = new Singleton();
		} else {
			fprintf(stderr, "%s\n", "All necessary instances are already created");
		}
		return p_instance;
	}
};
